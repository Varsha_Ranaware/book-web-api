﻿using Microsoft.AspNetCore.Mvc;
using WebApi2020.Models;
using WebApi2020.Repository;

namespace WebApi2020.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userRepository.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        [HttpPost("Register")]
        public async Task<ActionResult<int>> RegisterUser(User user)
        {
            var u = await _userRepository.Register(user); ;
            if (u != 0)
            {
                return Ok("User Added Successfully.");
            }
            return u;
        }
    }
}
