﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi2020.Models;
using WebApi2020.Repository;

namespace WebApi2020.Controllers
{
    
    [Route("[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartRepository _cartRepository;

        public CartController(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }
      
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Cart>> GetCartById(int id)
        {
          var cart=   await _cartRepository.GetCartById(id);
            if(cart == null)
            {
                return BadRequest("Cart Not Found.");
            }
            return Ok(cart);
        }

        [HttpDelete]
        [Authorize]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            var result =  await _cartRepository.Delete(id);
            if(result != true)
            {
                return BadRequest("Cart Not Found.");
            }
            return Ok(result);
        }

        [HttpPut]
        [Authorize]
        public async Task<ActionResult<Cart>> Edit(Cart cart)
        {
            var c =  await _cartRepository.Edit(cart);
            if(c == null)
            {
                return BadRequest("Cart Not Found.");
            }
            return Ok(c);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<int>> Add(Cart cart)
        {
            var id = await _cartRepository.Add(cart);
            if(id != 0)
            {
                return Ok("Cart Added Successfully.");
            }
            return id;
        }
    }
}
