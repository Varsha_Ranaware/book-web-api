﻿using Microsoft.AspNetCore.Mvc;
using WebApi2020.Models;
using WebApi2020.Repository;

namespace WebApi2020.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookRepository _bookRepository;

        public BookController(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Book>>> GetBookList()
        {
            return await _bookRepository.GetBookList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetBookById(int id)
        {
            return await _bookRepository.GetBookById(id);
        }
    }
}
