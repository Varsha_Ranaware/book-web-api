﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi2020.Models;
using WebApi2020.Repository;

namespace WebApi2020.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _OrderRepository;

        public OrderController(IOrderRepository OrderRepository)
        {
            _OrderRepository = OrderRepository;
        }
      
        [HttpGet("{id}")]
        //[Authorize]
        public async Task<ActionResult<Order>> GetOrderById(int id)
        {
            var Order = await _OrderRepository.GetOrderById(id);
            if (Order == null)
            {
                return BadRequest("Order Not Found.");
            }
            return Ok(Order);
        }

        [HttpDelete]
        //[Authorize]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            var Order = await _OrderRepository.Delete(id);
            if (Order != true)
            {
                return BadRequest("Order Not Found.");
            }
            return Ok(Order);
        }

        [HttpGet]
        //[Authorize]
        public async Task<ActionResult<List<Order>>> GetOrderList()
        {
            return await _OrderRepository.GetOrdersList();
        }

        [HttpPost]
        //[Authorize]
        public async Task<ActionResult<int>> Add(Order Order)
        {
            var OrderId = await _OrderRepository.Add(Order);
            if (OrderId != 0)
            {
                return Ok("Order Added Successfully.");
            }
            return OrderId;
        }
    }
}
