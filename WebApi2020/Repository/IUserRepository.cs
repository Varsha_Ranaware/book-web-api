﻿using WebApi2020.Models;

namespace WebApi2020.Repository
{       
    public interface IUserRepository
    {
            AuthenticateResponse Authenticate(AuthenticateRequest model);
            User GetById(int id);
            Task<int> Register(User user);
    }
}
