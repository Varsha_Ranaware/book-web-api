﻿using WebApi2020.Models;

namespace WebApi2020.Repository
{
    public interface IBookRepository
    {
        Task<List<Book>> GetBookList();
        Task<Book> GetBookById(int id);
    }
}
