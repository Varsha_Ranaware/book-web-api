﻿using WebApi2020.Models;

namespace WebApi2020.Repository
{
    public interface ICartRepository
    {
        Task<Cart> GetCartById(int id);
        Task<int> Add(Cart cart);
        Task<Cart> Edit(Cart cart);
        Task<bool> Delete(int id);
    }
}
