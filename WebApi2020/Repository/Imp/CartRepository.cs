﻿using Microsoft.EntityFrameworkCore;
using WebApi2020.Data;
using WebApi2020.Models;

namespace WebApi2020.Repository
{
    public class CartRepository : ICartRepository
    {
        private readonly BookDbContext context;
       

        public CartRepository(BookDbContext _context)
        {
            context = _context;
        }

        public async Task<int> Add(Cart cart)
        {
            if(cart != null)
            {
                await context.Cart.AddAsync(cart);
                await context.SaveChangesAsync();
                return cart.Id;
            }
            return 0;
        }
           

        public async Task<bool> Delete(int id)
        {
            var c = await context.Cart.FindAsync(id);
            if (c != null)
            {
                context.Cart.Remove(c);
                await context.SaveChangesAsync();
                return true;
            }
            return false;
            
        }

        public async Task<Cart> Edit(Cart cart)
        {
            var c = await context.Cart.Where(x => x.Id == cart.Id).FirstOrDefaultAsync();
            if (c is null) return null;
            c.Amount = cart.Amount; 
            c.Quantity = cart.Quantity; 
            c.UserId= cart.UserId;
            c.BookId = cart.BookId; 
            await context.SaveChangesAsync();
            return cart;
           
        }

       
        public async Task<Cart> GetCartById(int id)
        {
            return await context.Cart.Where(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
