﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApi2020.Models;
using WebApi2020.Data;
using WebApi2020.Helpers;
using Microsoft.Extensions.Options;

namespace WebApi2020.Repository.Imp
{
    public class UserRepository : IUserRepository
    {
        private readonly BookDbContext dbContext;
        private readonly AppSettings _appSettings;

        public UserRepository(IOptions<AppSettings> appSettings, BookDbContext _dbContext)
        {
            _appSettings = appSettings.Value;
            dbContext = _dbContext;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = dbContext.User.Where(x => x.Email == model.Username && x.Password == model.Password).FirstOrDefault();
            if (user == null) return null;

            var token = generateJwtToken(user);
            //if(token != null)
            //{
            //    HttpContext context;
            //    context.Items["Token"] = token;
            //}
            return new AuthenticateResponse(user, token);
        }


        public User GetById(int id)
        {
            return dbContext.User.Where(x => x.Id == id).FirstOrDefault();
        }

        // helper methods

        private string generateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }


        public async Task<int> Register(User user)
        {
            if (user != null)
            {
                await dbContext.User.AddAsync(user);
                await dbContext.SaveChangesAsync();
                return user.Id;
            }
            return 0;
        }
    }
}
