﻿using Microsoft.EntityFrameworkCore;
using WebApi2020.Data;
using WebApi2020.Models;

namespace WebApi2020.Repository
{
    public class BookRepository : IBookRepository
    {
        private readonly BookDbContext context;
       

        public BookRepository(BookDbContext _context)
        {
            context = _context;
        }

        public async Task<Book> GetBookById(int id)
        {
            var book = await context.Book.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (book is null) return null;
            return book;
        }

        public async Task<List<Book>> GetBookList()
        {
            return await context.Book.ToListAsync();
        }
    }
}
