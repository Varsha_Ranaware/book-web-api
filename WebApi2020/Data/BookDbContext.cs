﻿using Microsoft.EntityFrameworkCore;
using WebApi2020.Models;

namespace WebApi2020.Data
{
    public class BookDbContext : DbContext
    {
        public BookDbContext(DbContextOptions<BookDbContext> options) : base(options)
        {

        }
        public DbSet<User> User { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Book> Book { get; set; }
    }
}
