﻿using System.ComponentModel.DataAnnotations;

namespace WebApi2020.Models
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }
        public int Amount { get; set; }
        public Book Book { get; set; }
        public User User { get; set; }
    }
}
