﻿using System.ComponentModel.DataAnnotations;

namespace WebApi2020.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CartIds { get; set; }
        public int Total { get; set; }
    }
}
