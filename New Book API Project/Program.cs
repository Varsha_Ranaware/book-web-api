﻿using WebApi.Helpers;
using WebApi.Repository;
using WebApi.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// add services to DI container
{
    var services = builder.Services;
    services.AddCors();
    services.AddScoped<IUserRepository, UserRepository>();
    services.AddScoped<IBookRepository, BookRepository>();
    services.AddScoped<IOrderRepository, OrderRepository>();
    services.AddScoped<ICartRepository, CartRepository>();
    services.AddControllers();
    
    services.AddDbContext<BookDbContext>(options =>
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("BookDB"));
    });
    
     services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();
    services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));
   
    
    
}

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

{
    // global cors policy
    app.UseCors(x => x
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());

    // custom jwt auth middleware
    app.UseMiddleware<JwtMiddleware>();

    app.MapControllers();
}
app.UseHttpsRedirection();
app.UseAuthorization();
app.Run();