# dotnet-6-jwt-authentication-api

.NET 6.0 - JWT Authentication API

Documentation at https://jasonwatmore.com/post/2021/12/14/net-6-jwt-authentication-tutorial-with-example-api

https://localhost:44331/users/authenticate
{
    "Username":"Varsha",
    "Password":"123"
}

https://localhost:44331/users/Register
{
    "Name":"Apeksha",
    "MobileNumber":"1234567891",
    "Address":"Mumbai",
    "Email":"apeksha@gmail.com",
    "Password":"123"
}

GET : https://localhost:44331/order/1

{
	"id": 1,
    "userId": 1,
    "bookId": 1,
    "quantity": 2,
    "amount": 200
}

https://localhost:44331/cart/AddCart
{"userId":2,"bookId":1,"quantity":1,"amount":11200}`