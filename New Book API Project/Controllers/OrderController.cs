﻿using WebApi.Helpers;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private IOrderRepository _OrderRepository;

        public OrderController(IOrderRepository OrderRepository)
        {
            _OrderRepository = OrderRepository;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrderById(int id)
        {
            var Order = await _OrderRepository.GetOrderById(id);
            if (Order == null)
            {
                return BadRequest("Order Not Found.");
            }
            return Ok(Order);
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            var Order = await _OrderRepository.Delete(id);
            if (Order != true)
            {
                return BadRequest("Order Not Found.");
            }
            return Ok(Order);
        }

        [Authorize]
        [HttpGet("GetOrderList")]
        public async Task<ActionResult<List<Order>>> GetOrderList()
        {
            return await _OrderRepository.GetOrdersList();
        }

        [Authorize]
        [HttpPost("AddOrder")]
        public async Task<ActionResult<int>> Add(Order Order)
        {
            var OrderId = await _OrderRepository.Add(Order);
            if (OrderId != 0)
            {
                return Ok("Order Added Successfully.");
            }
            return OrderId;
        }
    }
}
