﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repository;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CartController : ControllerBase
    {
        private ICartRepository _cartRepository;

        public CartController(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<Cart>> GetCartById(int id)
        {
            var cart = await _cartRepository.GetCartById(id);
            if (cart == null)
            {
                return BadRequest("Cart Not Found.");
            }
            return cart;
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            var result = await _cartRepository.Delete(id);
            if (result != true)
            {
                return BadRequest("Cart Not Found.");
            }
            return Ok(result);
        }

        [Authorize]
        [HttpPut]
        public async Task<ActionResult<Cart>> Edit(Cart cart)
        {
            var c = await _cartRepository.Edit(cart);
            if (c == null)
            {
                return BadRequest("Cart Not Found.");
            }
            return Ok(c);
        }

        [Authorize]
        [HttpPost("AddCart")]
        public async Task<ActionResult<int>> Add(Cart cart)
        {
            var id = await _cartRepository.Add(cart);
            if (id != 0)
            {
                return Ok("Cart Added Successfully.");
            }
            return id;
        }
    }
}
