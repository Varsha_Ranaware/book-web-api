﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }
        public int Amount { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public Book Book { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public User User { get; set; }
    }
}
