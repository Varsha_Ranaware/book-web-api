﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int Price { get; set; }
        public int PublishYear { get; set; }
        public ICollection<Cart> Cart { get; set; }
    }
}
