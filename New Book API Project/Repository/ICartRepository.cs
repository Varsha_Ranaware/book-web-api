﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface ICartRepository
    {
        Task<Cart> GetCartById(int id);
        Task<int> Add(Cart cart);
        Task<Cart> Edit(Cart cart);
        Task<bool> Delete(int id);
    }
}
