﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IUserRepository
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        IEnumerable<User> GetAll();
        User GetById(int id);
        Task<int> Register(User user);
    }
}
