﻿using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly BookDbContext context;
       

        public OrderRepository(BookDbContext _context)
        {
            context = _context;
        }

        public async Task<List<Order>> GetOrdersList()
        {
            return await context.Order.ToListAsync();
        }

        public async Task<int> Add(Order Order)
        {
            if(Order != null)
            {
                await context.Order.AddAsync(Order);
                await context.SaveChangesAsync();
                return Order.Id;
            }
            return 0;
        }
           

        public async Task<bool> Delete(int id)
        {
            var c = await context.Order.FindAsync(id);
            if (c != null)
            {
                context.Order.Remove(c);
                await context.SaveChangesAsync();
                return true;
            }
            return false;
            
        }
       
        public async Task<Order> GetOrderById(int id)
        {
            return await context.Order.Where(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
