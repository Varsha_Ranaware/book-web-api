﻿using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class BookRepository : IBookRepository
    {
        private readonly BookDbContext context;
       

        public BookRepository(BookDbContext _context)
        {
            context = _context;
        }

        public async Task<Book> GetBookById(int id)
        {
            var book = await context.Book.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (book is null) return null;
            return book;
        }

        public async Task<List<Book>> GetBookList()
        {
            return await context.Book.ToListAsync();
        }
    }
}
