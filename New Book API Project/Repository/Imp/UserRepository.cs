﻿using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Data;
using WebApi.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using WebApi.Models;

namespace WebApi.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly BookDbContext context;
        
        private readonly AppSettings _appSettings;

        public UserRepository(IOptions<AppSettings> appSettings, BookDbContext _context)
        {
            _appSettings = appSettings.Value;
            context = _context;
        }

        public WebApi.Models.AuthenticateResponse Authenticate(WebApi.Models.AuthenticateRequest model)
        {
            var user = context.User.SingleOrDefault(x => x.Name == model.Username && x.Password == model.Password);

            if (user == null) return null;
            var token = generateJwtToken(user);

            return new WebApi.Models.AuthenticateResponse(user, token);
        }

        public IEnumerable<WebApi.Models.User> GetAll()
        {
            return context.User.ToList();
        }

        public WebApi.Models.User GetById(int id)
        {
            return context.User.FirstOrDefault(x => x.Id == id);
        }

        
        private string generateJwtToken(WebApi.Models.User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<int> Register(User user)
        {
            if (user != null)
            {
                await context.User.AddAsync(user);
                await context.SaveChangesAsync();
                return user.Id;
            }
            return 0;
        }
    }
}
