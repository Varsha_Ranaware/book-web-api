﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IBookRepository
    {
        Task<List<Book>> GetBookList();
        Task<Book> GetBookById(int id);
    }
}
