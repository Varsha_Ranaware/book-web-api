﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IOrderRepository
    {
        Task<Order> GetOrderById(int id);
        Task<int> Add(Order order);
        Task<bool> Delete(int id);
        Task<List<Order>> GetOrdersList();
    }
}
